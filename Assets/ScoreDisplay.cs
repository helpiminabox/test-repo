﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreDisplay : MonoBehaviour
{
	public GhostMovement velInfo;
	Text uiText;
	// Use this for initialization
	void Start()
	{
		uiText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update()
	{
		uiText.text = "Score: " + velInfo.score.ToString();
	}
}
