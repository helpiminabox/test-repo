﻿using UnityEngine;
using System.Collections;

public class GhostMovement : MonoBehaviour
{
	public float speed;
	public int score;
	int groundCollisions;
	Vector3 velocity;
	Rigidbody rigid;
	// Use this for initialization
	void Start()
	{
		velocity = Vector3.zero;
		groundCollisions = 0;
		rigid = GetComponent<Rigidbody>();
	}

	/// <summary>
	/// Make sure the spooky ghost can't move in air
	/// </summary>
	/// <param name="other">unspooky terrain trigger</param>
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.name == "ground")
		{
			groundCollisions++;
		}

		if(other.gameObject.name == "Coin")
		{
			score++;
			other.gameObject.transform.position = new Vector3(Random.Range(-8.9f, 10), 0, Random.Range(-5.6f, 6.3f));
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.gameObject.name == "ground")
		{
			groundCollisions--;
		}
	}

	/// <summary>
	/// Fixeds the update.
	/// </summary>
	void FixedUpdate()
	{
		if(groundCollisions >= 1)
		{
			float leftright = Input.GetAxis("Horizontal") * Time.fixedDeltaTime * speed;
			float updown = Input.GetAxis("Vertical") * Time.fixedDeltaTime * speed;
			rigid.AddForce(new Vector3(leftright, 0, updown));
			rigid.drag = 0.9f;
		}
		else
		{
			rigid.drag = 0;
		}
	}


	// Update is called once per frame
	void Update()
	{
		
	}
}
