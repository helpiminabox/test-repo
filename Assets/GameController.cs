﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour
{
	//Prefab objects
	public GameObject wall;
	public GameObject mob;
	public GameObject towerBase;
	public GameObject towerGun;
	public GameObject powerPlant;
	public GameObject gridCursor;

	//Anything that is confined to the grid
	List<GameObject> gridObjects;
	//Anything that can move freely
	List<GameObject> mobs;

	// Use this for initialization
	void Start()
	{
		gridObjects = new List<GameObject>();
		mobs = new List<GameObject>();

		for(int i = 0; i < 36; i++)
		{
			Instantiate(wall, new Vector3(-8.75f + i * 0.5f, 4.75f, 0), Quaternion.identity);
		}
	}
	
	// Update is called once per frame
	void Update()
	{
		Vector3 gridPos = (Camera.main.ScreenToWorldPoint(Input.mousePosition + new Vector3(0,0,10)))*2;
		gridPos = new Vector3(Mathf.Floor(gridPos.x), Mathf.Floor(gridPos.y), Mathf.Floor(gridPos.z)) / 2;
		gridCursor.transform.position = gridPos + new Vector3(0.25f, 0.25f, 0);
	}
}
